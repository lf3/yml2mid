# Secuenciador (MIDI) de información musical serializada (YAML).

Este trabajo forma parte del proyecto de investigación y tesis de grado
[Representación Textual de Estructuras Musicales y Entorno de Secuenciación](https://github.com/lifofernandez/UNQ-MyT-tesis/raw/master/output.pdf)
para la Licenciatura en Música y Tecnología de la Universidad Nacional de Quilmes.


## Instalación:
```
$ pip install -r requirements.txt
```

## Ejemplos de uso

### Melodía

```
$ ./yml2mid ejemplos/fc.yml 
```

La siguiente entrada:

```yaml
nombre: 'Feliz Cumpleaños'
base: &base 
  registro: [ 
    -12, -10, -8, -7, -5, -3, -1,
      0,   2,  4,  5,  7,  9, 11,
     12,  14, 15, 17, 19, 21, 23,
     24
  ]
  transportar: 60
  duraciones: [ 1.5 ]
  metro: 3/4
forma: [ 'espera', 'estrofa', 'estribo' ]
unidades: 
  espera:
    <<: *base
    dinamicas: [ 0 ] 
    duraciones: [ 2 ]
  a: &a 
    <<: *base
    alturas: [ 5, 5, 6, 5 ] 
    duraciones: [ .75, .25, 1, 1 ]
    letras: [ 'Que', 'los', 'cum', 'plas,' ]
  b: &b 
    <<: *base
    alturas: [ 8, 7 ] 
    duraciones: [ 1, 2 ]
    letras: [ 'fe', 'liz.' ]
  b`: 
    <<: *b
    transponer: 1
  a`: 
    <<: *a
    alturas: [ 5, 5, 12, 10 ] 
  a``: 
    <<: *a
    alturas: [ 8, 8, 7, 6 ] 
    letras: [ 'que', 'los', 'cum', 'plas,' ]
  a```: 
    <<: *a
    alturas: [ 11, 11, 10, 8 ] 
    letras: [ 'que', 'los', 'cum', 'plas,' ]
  A: 
    forma: [ 'a', 'b' ] 
  A`: 
    forma: [ 'a' , 'b`' ]
  B: 
    forma: [ 'a`', 'a``' ] 
  A``: 
    forma: [ 'a```', 'b`' ] 
  estrofa: 
    forma: [ 'A', 'A`' ]
  estribo: 
    forma: [ 'B', 'A``' ]
```

Produce un fichero MIDI,
cuya representación gráfica en formato partitura es la siguiente.

![Feliz Cumpleaños](extras/fc.svg)

### Multiples pistas

El programa acepta multiples definiciones de track
```
$ ./yml2mid ejemplos/bj-*.yml -o billie_jean -vv
```

```yaml
nombre: 'Billie Jean: Bass'
base: &base 
  registro: [ 
    -12, -10, -9, -7, -5, -3, -2,
      0,   2,  3,  5,  7,  9, 10,
     12,  14, 15, 17, 19, 21, 22,
     24
  ]
  transportar: 35 #B
  BPMs: [ 100 ]
  duraciones: [ .5 ]
  metro: 4/4
  dinamicas: [ 0.5 ]
  canal:  1 
  programas: [ 34 ]
unidades: 
  a:
    <<: *base
    alturas: [ 8, 5, 7, 8 ] 
  b:
    <<: *base
    alturas: [ 7, 5, 4, 5 ] 
  vuelta:
    forma: [ 'a', 'b' ]
    reiterar: 2
forma: [ 'vuelta' ] 
```

```yaml
nombre: 'Billie Jean: Drum'
base: &base 
  registracion: [ 36,38,44 ]
  transportar: 0 
  duraciones: [ .5 ]
  dinamicas: [ .5 ]
  BPMs: [ 100 ]
  canal: 9 
unidades: 
  a:
    <<: *base
    #alturas: [ 1,2,3 ]
    alturas: [ 3 ]
    voces:
      - [ 0 ]
      - [ -2 ]
  b:
    <<: *base
    alturas: [ 3 ]
  c:
    <<: *base
    alturas: [ 3 ]
    voces:
      - [ 0 ]
      - [ -2 ]
      - [ -1 ]
  vuelta:
    forma: [ 'a', 'b', 'c','b' ]
    reiterar: 4
forma: [ 'vuelta' ]
```

```yaml
nombre: 'Billie Jean: Keys'
base: &base 
  registracion: [ 
      0,   2,  3,  5,  7,  9, 10,
     12,  14, 15, 17, 19, 21, 22,
     24,  26, 27, 29, 31, 35, 38,
     46
  ]
  alturas: [ 1 ] 
  transportar: 59 #B
  duraciones: [ 1.5, 2.5 ]
  BPMs: [ 100 ]
  dinamicas: [ .7 ]
  canal: 0
  voces: 
    - [ 4 ]
    - [ 2 ]
    - [ 0 ]
unidades: 
  a:
    <<: *base
    alturas: [ 1, 2 ] 
  b:
    <<: *base
    alturas: [ 3, 2 ] 
  vuelta:
    forma: [ 'a', 'b' ]
forma: [ 'vuelta' ]
```

Las tres definiciones expuestas anteriormente, 
son combinadas en una única salida.

![Billie Jean](extras/bj.svg)


### Ayuda:

```
$ ./yml2mid -h 
```
