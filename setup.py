import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="yml2mid", # Replace with your own username
    version="0.0.1",
    author="Lisandro Fernández",
    author_email="lifofernandez@gmail.com",
    description="MIDI secuencer from YAML serialized data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lf3/yml2mid",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD 2-CLause License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    py_modules=['argumentos'],
    scripts=['yml2mid'] ,
    #entry_points={
    #    'console_scripts': [
    #        'yml2mid=yml2mid:main',
    #    ],
    #}, 
)

